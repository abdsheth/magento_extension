<?php
namespace Iflairwebtechnology\Log\Block\View;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Iflairwebtechnology\Log\Helper\Data
     */
    protected $logDataHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Iflairwebtechnology\Log\Helper\Data $logDataHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Iflairwebtechnology\Log\Helper\Data $logDataHelper,
        array $data = []
    )
    {
        $this->logDataHelper = $logDataHelper;
        parent::__construct($context, $data);
    }
    
    public function getFilePath()
    {        
        $path = $this->logDataHelper->getPath();
        return $path;
    }

    public function getFileName()
    {
        $params = $this->_request->getParams();
        return $params[0];
    }
}
