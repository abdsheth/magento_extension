<?php
namespace Iflairwebtechnology\Log\Controller\Adminhtml\Delete;

class Index extends AbstractLogPath
{
	public function getFilePathWithFile($fileName,$filePath)
    {
        $file = $filePath . $fileName;        
        $fp = fopen($file, "r+");
        ftruncate($fp, 0);
        fclose($fp);        
        $this->messageManager->addSuccessMessage(__("Delete File Content."));
        $this->_redirect('logfile/grid/index');
        return;
    }
}