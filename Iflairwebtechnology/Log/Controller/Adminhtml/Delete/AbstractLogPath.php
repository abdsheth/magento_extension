<?php
namespace Iflairwebtechnology\Log\Controller\Adminhtml\Delete;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Controller\Adminhtml\System;
use Magento\Framework\App\Response\Http\FileFactory;
use Iflairwebtechnology\Log\Helper\Data;
use Zend_Filter_BaseName;

abstract class AbstractLogPath extends System
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;
    /**
     * @var Data
     */
    protected $logDataHelper;

    public function __construct(Context $context, FileFactory $fileFactory, Data $logDataHelper)
    {
        $this->fileFactory = $fileFactory;
        $this->logDataHelper = $logDataHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        $param = $this->getRequest()->getParams();
        $path = $this->logDataHelper->getPath();
        $filePath = $this->getFilePathWithFile($param[0],$path);
    }

    /**
     * @param $filename
     * @param $path
     * @return string
     */
    abstract protected function getFilePathWithFile($filename,$path);
}