<?php
namespace Iflairwebtechnology\BulkDeleteExportedCSV\Controller\Adminhtml\Export;

 // * @category   Iflairwebtechnology
 // * @package    Iflairwebtechnology_ExportDelete
 // * @author     Iflairwebtechnology <nikung.panchal@iflair.com>
 // * @copyright  Copyright © Iflairwebtechnology All right reserved

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Exception\FileSystemException;
use \Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteFactory;
use \Magento\Framework\Message\ManagerInterface;


class MassDelete extends Action
{
    const URL = 'adminhtml/export_file/delete';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var WriteFactory
     */
    private $writeFactory;

    protected $_dir;

    protected $_messageManager;

    /**
     * Delete constructor.
     *
     * @param Action\Context $context
     * @param Filesystem $filesystem
     * @param WriteFactory $writeFactory
     */
    public function __construct(
            Action\Context $context, 
            Filesystem $filesystem,
            WriteFactory $writeFactory,
            DirectoryList $dir, 
            ManagerInterface $messageManager
    ) {
        $this->filesystem = $filesystem;
        $this->writeFactory = $writeFactory;
        parent::__construct($context);
        $this->_dir = $dir;
        $this->_messageManager = $messageManager;
    }

    public function execute()
    {
        $count = 0;
        $rootPath = $this->_dir->getRoot().'/var/export/';
        $csv_dir = scandir($rootPath, 1);
        if (array_key_exists("excluded", $this->getRequest()->getPostValue())) {
            print_r(__("all selected"));
            try { 
        
                foreach ($csv_dir as $file) {
                  if ($file != '..' && $file != '.') {
                      if (file_exists($rootPath . $file)) {   
                          unlink($rootPath . $file);
                          $count++;
                      } 
                      else {
                          print_r(__("File not found."));
                      }
                  }
                }
            }
            catch (\Exception $e) {
              $this->messageManager->addError(__('Record does not exist'));
            }
            $this->_messageManager->addSuccess(__("Successfully Deleted ").'<b>'.$count.'</b>'.' CSV File.');
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setRefererUrl();
            return $resultRedirect;
        }

        if (array_key_exists("selected", $this->getRequest()->getPostValue())) {
            print_r(__("particular selected"));

            try { 
 
                foreach ($this->getRequest()->getPostValue() ['selected'] as $value) {
                  if (file_exists($rootPath . $value)) {
                      unlink($rootPath . $value);
                      $count++;
                  } 
                  else {
                       print_r(__("File not found."));
                  }
                }

            } catch (\Exception $e) {
              $this->messageManager->addError(__('Record does not exist'));
            }
            $this->_messageManager->addSuccess(__("Successfully Deleted ").'<b>'.$count.'</b>'.' CSV File.');
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setRefererUrl();
            return $resultRedirect;
        }
    }
}