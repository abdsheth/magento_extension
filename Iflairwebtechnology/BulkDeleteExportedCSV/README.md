# Mage2 Module Iflairwebtechnology BulkDeleteExportedCSV

	 ``iflairwebtechnology/module-bulkdeleteexportedcsv``

## Main Functionalities

This extension allows you to delete and manage large amount of CSV which you have exported using Magento export feature which is no longer in use but never deleted. Extension helps to delete multiple CSV at the same time, which was exported previously. You can select particular CSV for deletion that will reduce the number of manual process. It saves time for admin user that it can delete multiple CSV at a time. The most important feature is it will free the system space which is occupied by CSV. It helps to delete unused CSV files from the previously done export process.

Exclusive Features
•	Clean up export CSV grid exported for Customer Addresses, Customers Main File, Products, Advanced Pricing
•	Single click to delete exported CSV from the list
•	Option to select all or the required one from the list and perform a mass deletion of CSV from the grid
•	Free the storage of CSV which not in use any more
•	After using this extension you can save your amount of server space
•	You can navigate to System >> Export and mass delete unused CSV


### Installation Steps

Step 1. To manually install the module, download the archive file.
Step 2. Unzip the file
Step 3. Create a folder [Magento_Root]/app/code/Iflairwebtechnology/BulkDeleteExportedCSV
Step 4. Drop/move the unzipped files to directory '[Magento_Root]/app/code/Iflairwebtechnology
/BulkDeleteExportedCSV



### Type 2: Composer

 - Install the module composer by running `composer require Iflairwebtechnology_BulkDeleteExportedCSV`
 - enable the module by running `php bin/magento module:enable Iflairwebtechnology_BulkDeleteExportedCSV`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Apply template changes by running `php bin/magento setup:static-content:deploy -f`
 - Flush the cache by running `php bin/magento cache:flush`

