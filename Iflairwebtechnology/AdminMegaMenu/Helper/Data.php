<?php
/*
 * @category  Iflairwebtechnology
 * @package   Iflairwebtechnology_AdminMegaMenu
 * @author    Iflairwebtechnology <nikunj.panchal@iflair.com>
 * @copyright Copyright © Iflairwebtechnology All right reserved
 */
 
namespace Iflairwebtechnology\AdminMegaMenu\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const XML_PATH_ENABLE = 'iflair_admin_megamenu/general/enable';
    
    public function isEnable()
    {
        $configValue = $this->scopeConfig->getValue(self::XML_PATH_ENABLE, ScopeInterface::SCOPE_STORE); // For Store
        
        return $configValue;
    }
}
