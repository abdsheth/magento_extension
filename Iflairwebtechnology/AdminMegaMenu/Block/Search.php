<?php
/*
 * @category  Iflairwebtechnology
 * @package   Iflairwebtechnology_AdminMegaMenu
 * @author    Iflairwebtechnology <nikunj.panchal@iflair.com>
 * @copyright Copyright © Iflairwebtechnology All right reserved
 */

namespace Iflairwebtechnology\AdminMegaMenu\Block;

use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Template;

class Search extends Template
{
    /**
     * CacheId
     */
    const CACHE_ID = 'IFLAIR_ADMINSEARCH_TABS';

    /**
     * Tabs
     *
     * @var \Magento\Config\Model\Config\Structure\Element\Iterator
     */
    protected $tabs;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $authSession;

    /**
     * @var string
     */
    protected $_template = 'Iflairwebtechnology_AdminMegaMenu::system/search.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Config\Model\Config\Structure $configStructure,
        SerializerInterface $serializer,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->tabs = $configStructure->getTabs();
        $this->serializer = $serializer;
        $this->authSession = $authSession;
    }

    /**
     * @return \Magento\User\Model\User
     */
    public function getCurrentUser()
    {
        return $this->authSession->getUser();
    }

    /**
     * Get all tabs
     *
     * @return \Magento\Config\Model\Config\Structure\Element\Iterator
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    public function prepareItem($item, &$result, $label = '', $value = '')
    {
        $_item = $item->getData();
        if (isset($_item['label'])) {
            if (isset($_item['_elementType']) && $_item['_elementType'] != 'section') {
                $value .= isset($_item['_elementType']) ? $_item['_elementType'] . '/' . $_item['id'] . '/' : $_item['id'] . '/';
            } else {
                $value = $_item['id'] . '/';
            }
            $result[] = [
                'label' => $label . $_item['label'],
                'value' => $value,
            ];
            $label .= $_item['label'] . '&nbsp;&nbsp;>&nbsp;&nbsp;';
        }
        if (isset($_item['children'])) {
            foreach ($item->getChildren() as $_item) {
                $this->prepareItem($_item, $result, $label, $value);
            }
        }
    }

    public function getCacheId()
    {
        return self::CACHE_ID . $this->getCurrentUser()->getId();
    }

    public function getSourceData()
    {
        $cacheId = $this->getCacheId();
        $data = $this->_cache->load($cacheId);
        if ($data) {
            return $data;
        }
        $result = [];
        foreach ($this->getTabs() as $k => $_tab) {
            $data = [];
            $this->prepareItem($_tab, $data);
            foreach ($data as $_item) {
                $result[] = $_item;
            }
        }
        $result = $this->serializer->serialize($result);
        $this->_cache->save(
            $result,
            $cacheId,
            [
                \Magento\Framework\App\Cache\Type\Config::TYPE_IDENTIFIER,
            ]
        );
        return $result;
    }
}
