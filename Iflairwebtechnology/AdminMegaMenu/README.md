# Mage2 Module Iflairwebtechnology AdminMegaMenu

    ``iflairwebtechnology/module-adminmegamenu``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Magento 2 AdminMegaMenu is an extension that allows you to navigate all useful configuration link from one menu tab and that will reduce the number of clicks. It saves time for admin user to go to each links from long navigation and help for quick changes. You can access most frequently used links, like Product, CMS page, Category, Customer, Widget, Theme configuration, Advance system, Catalog price rule from the one mega menu tab.

Provided search features over the Magento default admin search which is a powerful solution and provides a search box in the Magento back-end. The search result is a highly accurate, fast and flexible in the Magento back-end. By using the search box, the admin can easily find any menu, option, tab, field or any third party module.

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Iflairwebtechnology`
 - Enable the module by running `php bin/magento module:enable Iflair_AdminMegaMenu`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Apply template changes by running `php bin/magento setup:static-content:deploy -f`
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require iflairwebtechnology/module-adminmegamenu`
 - enable the module by running `php bin/magento module:enable Iflair_AdminMegaMenu`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Apply template changes by running `php bin/magento setup:static-content:deploy -f`
 - Flush the cache by running `php bin/magento cache:flush`


